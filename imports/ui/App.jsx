import React from 'react';
import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom'

export const AuthLayout = (props) => (
    <div>
        <div>Auth header</div>
        {props.children}
        <div>Auth footer</div>
    </div>
);

export const NotAuthLayout = (props) => (
    <>
        <div>Not auth header</div>
        {props.children}
        <div>Not auth footer</div>
    </>
)


export const MainLayout = (props) => (
    <>
        <div>Main header</div>
        {props.children}
        <div>Main footer</div>
    </>
)

const DashboardPage = () => (
    <>Dashboard <Link to={'/about'}>About</Link></>
)
const CategoryPage = () => (
    <>CategoryPage</>
)
const LoginPage = () => (
    <NotAuthLayout>
        <>LoginPage</>
    </NotAuthLayout>

)


const NotAuthRoute = ({component: Component, ...rest}) => {
    const token = null
    return (
        <Route {...rest} render={props => (
            <NotAuthLayout>
                <Component {...props} />
            </NotAuthLayout>
        )}/>
    )
}
const AuthRoute = ({component: Component, ...rest}) => {
    const token = "token";
    return (
        <>
            {token ? <Route {...rest} render={props => (
                <AuthLayout>
                    <Component {...props} />
                </AuthLayout>
            )}/> : undefined}
        </>
    )
}

export const Header = () => (
    <div>Header</div>
)
export const Footer = () => (
    <div>Footer</div>
)
export const RouteComponent = () => {
    return (
        <Router>
            {/*V1*/}
            {/*<Switch>
               <Route path='/login' component={LoginPage} />

               <Route path='/admin/:path?' exact>
                   <AuthLayout>
                       <Switch>
                           <Route path='/admin' exact component={DashboardPage} />
                           <Route path='/admin/setting' component={CategoryPage} />
                       </Switch>
                   </AuthLayout>
               </Route>

               <Route>
                   <NotAuthLayout>
                       <Switch>
                           <Route path='/' exact component={DashboardPage} />
                           <Route path='/about' component={CategoryPage} />
                       </Switch>
                   </NotAuthLayout>
               </Route>


           </Switch>*/}


            {/*V2*/}
            {/* <Switch>
                <AuthRoute path='/admin' exact component={DashboardPage}/>
                <AuthRoute path='/admin/setting' component={CategoryPage}/>
                <NotAuthRoute path={'/'} exact component={DashboardPage}/>
                <NotAuthRoute path='/about' component={CategoryPage}/>
                <NotAuthRoute path={'/login'} component={LoginPage}/>
            </Switch>*/}


            {/*V3*/}
            <MainLayout>
                <Switch>
                    <Route path={'/'} exact component={DashboardPage}/>
                    <Route path={'/about'} exact component={LoginPage}/>
                </Switch>
            </MainLayout>
        </Router>
    )
}
