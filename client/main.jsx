import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { RouteComponent } from '/imports/ui/App';

Meteor.startup(() => {
  render(<RouteComponent/>, document.getElementById('react-target'));
});
